/**
 * Created by fabianotavares on 26/01/18.
 */


$(document).ready(function () {
    $(function () {
        $('.etl-cron').jqCron({
            enabled_minute: true,
            multiple_dom: true,
            multiple_month: true,
            multiple_mins: true,
            multiple_dow: true,
            multiple_time_hours: true,
            multiple_time_minutes: true,
            default_value: '0 0 * * *',
            no_reset_button: false,
            lang: 'pt_br'
        });
    });
});