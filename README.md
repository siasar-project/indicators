Antes de comenzar este proyecto, debe tener el SIASAR-BI clonado y PENTAHO DATA INTEGRATION.
1. Copiar y pegar el config.example.json y poner la ruta del SIASAR-BI y dónde está instalado Pentaho Data Itegration.
Por ejemplo
-"siasar_etl":"/home/usuario/SIASAR-BI",
-"pentaho_pdi":"/home/usuario/pentaho/data-integration"

2. Instalar python3.

3. Instalar dependencia python3-pip.

4. Luego realizamos la instalación de los requerimientos
pip3 install -r requirements.txt

5. Correr servicio proyecto.
python3 manage.py runserver
