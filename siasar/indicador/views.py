from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets

from siasar.indicador.models import VariavelDw
from siasar.indicador.serializers import VariavelDwSerializer


class VariavelDwViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = VariavelDw.objects.all().order_by('-vardw_seq')
    serializer_class = VariavelDwSerializer