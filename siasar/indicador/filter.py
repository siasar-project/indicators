'''
Created on 24 de nov de 2016

@author: fabianotavares
'''
from django.contrib.admin.filters import SimpleListFilter
from django.db.models import Q
from siasar.indicador.models import Variavel
from siasar.dados.models import TbTipoContexto


class ClassificaFilter(SimpleListFilter):
    title = 'Classificação' # or use _('country') for translated title
    parameter_name = 'classifica'

    def lookups(self, request, model_admin):
            
        list_res = set()
        list_res.add(('a', 'A'))
        list_res.add(('b', 'B'))
        list_res.add(('c', 'C'))
        list_res.add(('d', 'D'))
        list_res.add(('x', '-'))
            
        return sorted(list_res)
        # You can also use hardcoded model name like "Country" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        
        if self.value():
            valor =  self.value()
            if(valor!=None):
                if(valor=='d'): return queryset.filter(vardw_valor__gte=0.,vardw_valor__lte=0.4,varia_seq__varia_tipo='IND')
                elif(valor=='c'): return queryset.filter(vardw_valor__gt=0.4,vardw_valor__lte=0.7,varia_seq__varia_tipo='IND')
                elif(valor=='b'): return queryset.filter(vardw_valor__gt=0.7,vardw_valor__lte=0.9,varia_seq__varia_tipo='IND')
                elif(valor=='a'): return queryset.filter(vardw_valor__gt=0.9,vardw_valor__lte=1.0,varia_seq__varia_tipo='IND')
                else: return queryset.filter(vardw_valor=None,varia_seq__varia_tipo='IND')
            else:
                return queryset.filter(vardw_valor=None)
            
        else:
            return queryset

class ContextoFilter(SimpleListFilter):
    title = 'Contexto' # or use _('country') for translated title
    parameter_name = 'context'

    def lookups(self, request, model_admin):
            
        list_context = TbTipoContexto.objects.filter(tipo_con_seq_pai=None)
        list_res = set()
        
        for contxt in list_context:
            list_res.add((contxt.tipo_con_seq, contxt.tipo_con_nome))
            
        return sorted(list_res)
        
            
        return sorted(list_res)
        # You can also use hardcoded model name like "Country" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
            if self.value():
                valor =  self.value()
                if(valor):
                    return queryset.filter(tipo_con_seq=valor)
                else:
                    return queryset.all()
            else:
                return queryset


class IndicadorFilter(SimpleListFilter):
    title = 'Indicadores' # or use _('country') for translated title
    parameter_name = 'indic'

    def lookups(self, request, model_admin):
        
        list_varia = []
        if('tipo_con_seq__tipo_con_seq__exact' in request.GET.keys()):
            list_varia = Variavel.objects.filter(varia_ativo=True, tipo_con_seq__tipo_con_seq__exact=int(request.GET['tipo_con_seq__tipo_con_seq__exact']), varia_tipo='IND')
        else:
            list_varia = Variavel.objects.filter(varia_ativo=True, varia_tipo='IND')
        list_res = set()
        
        for varia in list_varia:
            list_res.add((varia.varia_seq, varia.varia_nome))
            
        return sorted(list_res,key=lambda tup: tup[1])
        # You can also use hardcoded model name like "Country" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
         
        if self.value():
            valor =  self.value()
            if(valor):
                return queryset.filter(varia_seq=valor)
            else:
                return queryset.all()
             
        else:
            return queryset



class VariavelBasicaFilter(SimpleListFilter):
    title = 'Tipo de Variavel' # or use _('country') for translated title
    parameter_name = 'tipo_varia'

    def lookups(self, request, model_admin):
        
        list_res = set()
        list_res.add((1, 'Basica'))
        list_res.add((2, 'Composta'))
        #list_res.add((3, 'Indefinida'))
            
        return sorted(list_res)
        # You can also use hardcoded model name like "Country" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            valor =  self.value()
            
            if(valor!=None):
                if(valor=='1'): return queryset.filter(~Q(varia_consulta=None)).filter(~Q(varia_consulta=''))
                elif(valor=='2'): return queryset.filter(~Q(varia_formula=None)).filter(~Q(varia_formula=''))
                #elif(valor=='3'): return queryset.exclude(~Q(varia_consulta=None)).exclude(~Q(varia_consulta='')).exclude(~Q(varia_formula=None)).exclude(~Q(varia_formula=''))
            else:
                return queryset
            
        else:
            return queryset
