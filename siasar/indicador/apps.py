from django.apps import AppConfig


class IndicadorConfig(AppConfig):
    name = 'siasar.indicador'
    verbose_name= 'Indicadores'
