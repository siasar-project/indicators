'''
Created on 20 de out de 2016

@author: fabianotavares
'''
from django import forms
from django.contrib import admin

from siasar.codemirror.widgets import CodeMirrorTextarea
from siasar.indicador.filter import ClassificaFilter, VariavelBasicaFilter, \
    IndicadorFilter, ContextoFilter
from siasar.indicador.models import Variavel, \
    VariavelDw, VariaRestricao, VariaVaria, LogCalculo
from siasar.util import SqlWidget, ReadOnlyAdmin

codemirror_widget = CodeMirrorTextarea(
    mode="pig",
    mime="text/x-pig",
    theme="default",
    addon_js=('edit/matchbrackets', 'edit/closebrackets'),
    js_var_format='ed_%s',

    config={
        'fixedGutter': True,
        'lineNumbers': True,
        'matchBrackets': True,
        'autoCloseBrackets': True,
    },
)
sql_widget = SqlWidget(
    mode="sql",
    mime="text/x-plsql",
    theme="default",
    addon_js=('edit/matchbrackets', 'edit/closebrackets'),
    js_var_format='sql_%s',

    config={
        'fixedGutter': True,
        'lineNumbers': True,
        'matchBrackets': True,
        'autoCloseBrackets': True,
    },
)


class RestricaoForm(forms.ModelForm):
    vares_formula = forms.CharField(label='Formula', widget=codemirror_widget, required=False)

    class Meta:
        model = VariaRestricao
        exclude = []

class VariavelDwAdmin(admin.ModelAdmin):
    list_display = ('get_variavel', 'vardw_valor', 'classifica', 'get_localidade', 'vardw_tempo','data_calculo', 'vardw_erro')
    search_fields = ('varia_seq__varia_nome', 'vardw_localidade__tda_nome',)
    raw_id_fields = ('vardw_localidade',)
    readonly_fields = ('data_calculo',)
    list_filter = ('vardw_localidade__tda_pais','varia_seq__varia_tipo', ClassificaFilter, 'tipo_con_seq', 'varia_seq__varia_ativo', IndicadorFilter)
    actions = ['delete_selected']


class VariaVariavelAdmin(admin.ModelAdmin):
    list_display = ('varia_pai', 'varia_filho')
    search_fields = ('varia_pai__varia_nome', 'varia_filho__varia_nome',)


class VariaRestricaoAdmin(admin.ModelAdmin):
    list_display = ('get_variavel', 'vares_formula', 'vares_valor', 'vares_erro', 'vares_ativo')
    search_fields = ('varia_seq__varia_nome',)
    list_filter = ('vares_ativo',)
    form = RestricaoForm


class VariaIndicAdmin(admin.ModelAdmin):
    list_display = ('indic_seq', 'varia_seq')
    search_fields = ('indic_seq__indic_codigo', 'varia_seq__varia_nome',)


class VariavelForm(forms.ModelForm):
    varia_consulta = forms.CharField(label='Consulta', widget=sql_widget, required=False)
    varia_formula = forms.CharField(widget=codemirror_widget, required=False)

    class Meta:
        model = Variavel
        exclude = []


class VariavelAdmin(admin.ModelAdmin):
    list_display = ('varia_nome', 'get_consulta_formula', 'varia_descricao', 'varia_ativo')
    search_fields = ('varia_nome',)
    list_filter = ('varia_tipo', 'tipo_con_seq', 'varia_ativo', 'varia_nivel',)
    actions = ['make_ativo', 'make_desativo']

    change_form_template = 'admin/siasar/indicador/change_form.html'
    form = VariavelForm

    def make_ativo(self, request, queryset):
        queryset.filter(varia_ativo=False).update(varia_ativo=True)
        queryset.filter(varia_ativo=None).update(varia_ativo=True)

    make_ativo.short_description = "Ativar Status"

    def make_desativo(self, request, queryset):
        queryset.filter(varia_ativo=True).update(varia_ativo=False)
        queryset.filter(varia_ativo=None).update(varia_ativo=False)

    make_desativo.short_description = "Desativar Status"


class LogErrorAdmin(ReadOnlyAdmin):
    search_fields = ('logcal_nome_var', 'logcal_erro_valor')
    list_display = ('logcal_nome_var', 'logcal_erro_valor', 'logcal_data', 'logcal_erro_reparado')
    list_filter = ('logcal_erro_reparado',)
    actions = ['delete_selected', 'make_ativo', 'make_desativo']

    def has_delete_permission(self, request, obj=None):
        return True

    def make_ativo(self, request, queryset):
        queryset.filter(logcal_erro_reparado=False).update(logcal_erro_reparado=True)
        queryset.filter(logcal_erro_reparado=None).update(logcal_erro_reparado=True)

    make_ativo.short_description = "Ativar Status"

    def make_desativo(self, request, queryset):
        queryset.filter(logcal_erro_reparado=True).update(logcal_erro_reparado=False)
        queryset.filter(logcal_erro_reparado=None).update(logcal_erro_reparado=False)

    make_desativo.short_description = "Desativar Status"


admin.site.register(LogCalculo, LogErrorAdmin)
admin.site.register(Variavel, VariavelAdmin)
admin.site.register(VariavelDw, VariavelDwAdmin)
admin.site.register(VariaRestricao, VariaRestricaoAdmin)
admin.site.register(VariaVaria, VariaVariavelAdmin)


def clean_variavel_dw(pais):
    print("Deleted VariavelDW for " + pais)
    VariavelDw.objects.filter(vardw_localidade__tda_pais__exact=pais).delete()
