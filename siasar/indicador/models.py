# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models

from siasar.dados.models import TbUnit, TbTipoContexto, TbDivisaoAdmin
from siasar.util import valida_formula, valida_sql


class LogCalculo(models.Model):
    logcal_seq = models.AutoField(primary_key=True)
    logcal_nome_var = models.CharField(max_length=255)
    logcal_cmd_exec = models.TextField(blank=True, null=True)
    logcal_erro_valor = models.TextField(blank=True, null=True)
    logcal_erro_result = models.TextField(blank=True, null=True)
    logcal_erro_reparado = models.NullBooleanField()
    logcal_data = models.DateTimeField()
    
    class Meta:
        app_label = 'indicador'
        db_table = 'tb_log_calculo'
        verbose_name = 'Log de Erro'
        
    def __str__(self):
        return '' + self.logcal_nome_var


class VariaVaria(models.Model):
    vavaria_seq = models.AutoField(primary_key=True)
    varia_pai = models.ForeignKey('indicador.Variavel', models.DO_NOTHING, db_column='varia_pai', blank=True, null=True)
    varia_filho = models.ForeignKey('indicador.Variavel',models.DO_NOTHING, db_column='varia_filho', related_name="VariaVaria.varia_filho+", blank=True, null=True)

    class Meta:
        app_label = 'indicador'
        db_table = 'tb_variavaria'
        verbose_name = 'Variavel de Variavel'
        verbose_name_plural = 'Variaveis e Variaveis'
    
    def __str__(self):
        return '' + self.varia_pai.varia_nome+' -> '+ self.varia_filho.varia_nome


class Variavel(models.Model):
    TIPO_VAR_CHOICES = (
        ('VAR','Variável'),
        ('IND','Indicador'),
    )
    varia_seq = models.AutoField(primary_key=True)
    varia_nome = models.CharField(max_length=255)
    varia_descricao = models.CharField(max_length=600)
    varia_consulta = models.TextField(blank=True, null=True)#,validators=[valida_sql])
    unit_seq = models.ForeignKey(TbUnit,db_column='unit_seq', blank=True, null=True)
    varia_formula = models.TextField(blank=True, null=True,validators=[valida_formula])
    tipo_con_seq = models.ForeignKey(TbTipoContexto, models.DO_NOTHING, db_column='tipo_con_seq',verbose_name="Contexto")
    varia_ativo = models.NullBooleanField()
    varia_tipo = models.CharField(max_length=3,verbose_name="Tipo Variável", choices=TIPO_VAR_CHOICES)
    varia_nivel = models.IntegerField(blank=True, null=True)

    class Meta:
        app_label = 'indicador'
        db_table = 'tb_variavel'
        verbose_name = 'Variavel'
        verbose_name_plural = 'Variaveis'
        ordering = ['varia_nome']
    
    def get_consulta_formula(self):
        if(self.varia_formula):
            return self.varia_formula
        else:
            return self.varia_consulta
    get_consulta_formula.short_description = 'Consulta/Formula'

        
    def __str__(self):
        if(self.varia_formula):
            return '' + self.varia_nome+ " = "+self.varia_formula
        else:
            return '' + self.varia_nome


class VariavelDw(models.Model):
    vardw_seq = models.BigAutoField(primary_key=True)
    varia_seq = models.ForeignKey('indicador.Variavel', models.DO_NOTHING, db_column='varia_seq', blank=True, null=True)
    vardw_valor = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    vardw_entidade = models.BigIntegerField()
    vardw_localidade = models.ForeignKey(TbDivisaoAdmin, models.DO_NOTHING, db_column='vardw_localidade', blank=True, null=True)
    vardw_tempo = models.DateField(blank=True, null=True)
    tipo_con_seq = models.ForeignKey(TbTipoContexto, models.DO_NOTHING,db_column='tipo_con_seq', blank=True, null=True)
    vardw_erro = models.TextField(blank=True, null=True)
    data_calculo = models.DateTimeField(blank=True, null=True)
        
    def get_variavel(self):
        return self.varia_seq.varia_nome
    get_variavel.short_description = 'Variavel/Indicador'
    get_variavel.admin_order_field = 'varia_seq__varia_nome'
    
    def get_localidade(self):
        if(self.vardw_localidade):
            return self.vardw_localidade.tda_nome
        else:
            return ''
    get_localidade.short_description = 'Localidade'
    get_localidade.admin_order_field = 'vardw_localidade__tda_nome'
    
    class Meta:
        app_label = 'indicador'
        db_table = 'tb_variavel_dw'
        verbose_name = 'Variavel DW'
        
    def classifica(self):
        if(self.varia_seq.varia_tipo=='IND' and self.vardw_valor):
            if(self.vardw_valor<=0.40): return '<b style="color:red">D</b>'
            elif(self.vardw_valor<=0.70): return '<b style="color:orange">C</b>'
            elif(self.vardw_valor<=0.90): return '<b style="color:yellow">B</b>'
            else: return '<b style="color:green">A</b>'
        else:
            return '-'
    classifica.allow_tags=True
        
        
    def __str__(self):
        return self.vardw_localidade.tda_nome + ' | ' + self.varia_seq.varia_nome+' = '+str(self.vardw_valor) 
    

class VariaRestricao(models.Model):
    vares_seq = models.BigAutoField(primary_key=True)
    varia_seq = models.ForeignKey('indicador.Variavel', models.DO_NOTHING, db_column='varia_seq', blank=True, null=True)
    vares_formula = models.TextField(blank=True, null=True,validators=[valida_formula])
    vares_valor = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    vares_erro = models.TextField(blank=True, null=True)
    vares_ativo = models.NullBooleanField()
    
    def get_variavel(self):
        return self.varia_seq.varia_nome
    get_variavel.short_description = 'Variavel'
    get_variavel.admin_order_field = 'varia_seq__varia_nome'

    class Meta:
        app_label = 'indicador'
        db_table = 'tb_varia_restricao'
        verbose_name = 'Restrição'
        verbose_name_plural = 'Restrições'
        
    def __str__(self):
        return '' + self.vares_formula


