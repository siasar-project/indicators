from rest_framework import serializers

from siasar.indicador.models import VariavelDw, Variavel


class VariavelSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Variavel
        fields = ('varia_seq', 'varia_nome')



class VariavelDwSerializer(serializers.HyperlinkedModelSerializer):

    varia_seq = VariavelSerializer(read_only=True)
    class Meta:
        model = VariavelDw
        fields = ('vardw_seq', 'varia_seq', 'vardw_valor')#, 'vardw_entidade','vardw_localidade','vardw_tempo','tipo_con_seq','vardw_erro')
