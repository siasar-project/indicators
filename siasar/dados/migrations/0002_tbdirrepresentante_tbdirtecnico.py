# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-03-03 13:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dados', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='TbDirRepresentante',
            fields=[
                ('drep_seq', models.IntegerField(primary_key=True, serialize=False)),
                ('sep_b_007_001', models.CharField(max_length=255)),
                ('sep_b_009_001', models.CharField(blank=True, max_length=60, null=True)),
            ],
            options={
                'db_table': 'tb_dir_representante',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='TbDirTecnico',
            fields=[
                ('dtec_seq', models.IntegerField(primary_key=True, serialize=False)),
                ('sep_b_011_001', models.CharField(max_length=255)),
                ('sep_b_013_001', models.CharField(blank=True, max_length=60, null=True)),
            ],
            options={
                'db_table': 'tb_dir_tecnico',
                'managed': False,
            },
        ),
    ]
