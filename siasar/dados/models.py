from django.db import models


class TbDivisaoAdmin(models.Model):
    tda_seq = models.IntegerField(primary_key=True)
    tda_nome = models.CharField(max_length=255)
    tda_pai = models.ForeignKey('TbDivisaoAdmin', db_column='tda_pai',blank=True, null=True)
    tda_pais = models.CharField(max_length=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tb_divisao_admin'
        verbose_name = 'Divisao Admin'
        
    def __str__(self):
        return '' + self.tda_nome


class TbLocalidade(models.Model):
    loc_seq = models.AutoField(primary_key=True)
    loc_nome = models.CharField(max_length=1500)

    class Meta:
        managed = False
        db_table = 'tb_localidade'
        
    def __str__(self):
        return '' + self.loc_nome 

class TbPat(models.Model):
    pat_seq = models.IntegerField(primary_key=True)
    validado = models.NullBooleanField()
    data_validacao = models.DateTimeField(blank=True, null=True)
    pat_a_001_001 = models.CharField(max_length=50, blank=True, null=True)
    pat_a_002_001 = models.DateField(blank=True, null=True)
    pat_a_003_001 = models.CharField(max_length=80, blank=True, null=True)
    pat_a_004_001 = models.CharField(max_length=1000, blank=True, null=True)
    pat_a_005_001 = models.CharField(max_length=300, blank=True, null=True)
    pat_a_006_001 = models.IntegerField(blank=True, null=True)
    pat_a_007_001 = models.IntegerField(blank=True, null=True)
    pat_a_008_001 = models.IntegerField(blank=True, null=True)
    pat_a_009_001 = models.CharField(max_length=2, blank=True, null=True)
    pat_b_001_001 = models.IntegerField(blank=True, null=True)
    pat_b_002_001 = models.IntegerField(blank=True, null=True)
    pat_c_001_001 = models.IntegerField(blank=True, null=True)
    pat_c_002_001 = models.NullBooleanField()
    pat_c_003_001 = models.IntegerField(blank=True, null=True)
    pat_c_004_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    pat_c_005_001 = models.IntegerField(blank=True, null=True)
    pat_c_006_001 = models.IntegerField(blank=True, null=True)
    pat_c_007_001 = models.IntegerField(blank=True, null=True)
    pat_c_008_001 = models.IntegerField(blank=True, null=True)
    pat_c_009_001 = models.IntegerField(blank=True, null=True)
    pat_c_010_001 = models.IntegerField(blank=True, null=True)
    pat_c_011_001 = models.NullBooleanField()
    pat_c_012_001 = models.IntegerField(blank=True, null=True)
    pat_c_013_001 = models.NullBooleanField()
    pat_c_014_001 = models.IntegerField(blank=True, null=True)
    pat_c_015_001 = models.NullBooleanField()
    pat_c_016_001 = models.IntegerField(blank=True, null=True)
    pat_c_017_001 = models.NullBooleanField()
    pat_c_018_001 = models.IntegerField(blank=True, null=True)
    pat_d_001_001 = models.IntegerField(blank=True, null=True)
    pat_d_002_001 = models.IntegerField(blank=True, null=True)
    pat_d_003_001 = models.IntegerField(blank=True, null=True)
    pat_d_004_001 = models.IntegerField(blank=True, null=True)
    pat_d_005_001 = models.IntegerField(blank=True, null=True)
    pat_d_006_001 = models.IntegerField(blank=True, null=True)
    pat_d_007_001 = models.IntegerField(blank=True, null=True)
    pat_d_008_001 = models.IntegerField(blank=True, null=True)
    pat_d_009_001 = models.IntegerField(blank=True, null=True)
    pat_d_010_001 = models.IntegerField(blank=True, null=True)
    pat_d_011_001 = models.IntegerField(blank=True, null=True)
    pat_d_012_001 = models.IntegerField(blank=True, null=True)
    pat_d_013_001 = models.CharField(max_length=3000, blank=True, null=True)
    pat_d_014_001 = models.IntegerField(blank=True, null=True)
    pat_e_001_001 = models.CharField(max_length=4000, blank=True, null=True)
    pais_sigla = models.CharField(max_length=2, blank=True, null=True)
    date_last_update = models.DateTimeField(blank=True, null=True)
    pat_a_010_001 = models.FloatField(blank=True, null=True)
    pat_a_011_001 = models.FloatField(blank=True, null=True)
    
    def __str__(self):
        return '' + self.pat_a_005_001
    
    class Meta:
        app_label='dados'
        managed = False
        db_table = 'tb_pat'
        verbose_name= 'Prestador de Assistência Técnica'
        verbose_name_plural= 'PATs'



class TbPrestServico(models.Model):
    prser_seq = models.BigIntegerField(primary_key=True)
    data_validacao = models.DateTimeField(blank=True, null=True)
    validado = models.NullBooleanField()
    sep_a_001_001 = models.TextField(blank=True, null=True)
    sep_a_002_001 = models.IntegerField(blank=True, null=True)
    sep_a_003_001 = models.IntegerField(blank=True, null=True)
    sep_a_004_001 = models.IntegerField(blank=True, null=True)
    sep_a_005_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_a_006_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_a_007_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_a_008_001 = models.CharField(max_length=100, blank=True, null=True)
    sep_a_009_001 = models.IntegerField(blank=True, null=True)
    sep_a_010_001 = models.TextField(blank=True, null=True)
    sep_b_001_001 = models.DateField(blank=True, null=True)
    sep_b_002_001 = models.IntegerField(blank=True, null=True)
    sep_b_003_001 = models.DateField(blank=True, null=True)
    sep_b_004_001 = models.NullBooleanField()
    sep_b_005_001 = models.IntegerField(blank=True, null=True)
    sep_b_014_001 = models.NullBooleanField()
    sep_b_015_001 = models.NullBooleanField()
    sep_b_016_001 = models.NullBooleanField()
    sep_c_001_001 = models.NullBooleanField()
    sep_c_005_001 = models.IntegerField(blank=True, null=True)
    sep_c_007_001 = models.NullBooleanField()
    sep_c_009_001 = models.IntegerField(blank=True, null=True)
    sep_c_011_001 = models.IntegerField(blank=True, null=True)
    sep_c_012_001 = models.IntegerField(blank=True, null=True)
    sep_c_014_001 = models.IntegerField(blank=True, null=True)
    sep_c_015_001 = models.IntegerField(blank=True, null=True)
    sep_c_017_001 = models.IntegerField(blank=True, null=True)
    sep_c_018_001 = models.IntegerField(blank=True, null=True)
    sep_c_019_001 = models.TextField(blank=True, null=True)
    sep_d_001_001 = models.NullBooleanField()
    sep_d_002_001 = models.IntegerField(blank=True, null=True)
    sep_d_003_001 = models.IntegerField(blank=True, null=True)
    sep_d_004_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_e_001_001 = models.IntegerField(blank=True, null=True)
    sep_e_002_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_e_003_001 = models.IntegerField(blank=True, null=True)
    sep_e_004_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_e_005_001 = models.IntegerField(blank=True, null=True)
    sep_e_006_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_e_007_001 = models.IntegerField(blank=True, null=True)
    sep_e_008_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_e_009_001 = models.IntegerField(blank=True, null=True)
    sep_e_010_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_e_012_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_e_013_001 = models.IntegerField(blank=True, null=True)
    sep_e_014_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_e_015_001 = models.IntegerField(blank=True, null=True)
    sep_e_016_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_e_018_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_e_019_001 = models.IntegerField(blank=True, null=True)
    sep_e_020_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_f_001_001 = models.NullBooleanField()
    sep_f_003_001 = models.IntegerField(blank=True, null=True)
    sep_f_004_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_f_005_001 = models.IntegerField(blank=True, null=True)
    sep_f_006_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_f_007_001 = models.NullBooleanField()
    sep_f_008_001 = models.IntegerField(blank=True, null=True)
    sep_f_009_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_f_010_001 = models.NullBooleanField()
    sep_f_011_001 = models.IntegerField(blank=True, null=True)
    sep_f_012_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_f_013_001 = models.IntegerField(blank=True, null=True)
    sep_f_014_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_f_015_001 = models.IntegerField(blank=True, null=True)
    sep_f_016_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_f_017_001 = models.IntegerField(blank=True, null=True)
    sep_f_018_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sep_g_001_001 = models.IntegerField(blank=True, null=True)
    sep_g_002_001 = models.NullBooleanField()
    sep_g_003_001 = models.NullBooleanField()
    sep_g_004_001 = models.IntegerField(blank=True, null=True)
    sep_h_001_001 = models.NullBooleanField()
    sep_h_002_001 = models.TextField(blank=True, null=True)
    sep_h_003_001 = models.NullBooleanField()
    sep_h_004_001 = models.NullBooleanField()
    sep_h_005_001 = models.IntegerField(blank=True, null=True)
    sep_h_006_001 = models.IntegerField(blank=True, null=True)
    sep_h_007_001 = models.IntegerField(blank=True, null=True)
    sep_h_008_001 = models.IntegerField(blank=True, null=True)
    sep_h_009_001 = models.IntegerField(blank=True, null=True)
    sep_h_010_001 = models.IntegerField(blank=True, null=True)
    sep_h_011_001 = models.IntegerField(blank=True, null=True)
    sep_h_012_001 = models.IntegerField(blank=True, null=True)
    sep_h_013_001 = models.IntegerField(blank=True, null=True)
    sep_h_014_001 = models.IntegerField(blank=True, null=True)
    sep_h_015_001 = models.IntegerField(blank=True, null=True)
    sep_h_016_001 = models.IntegerField(blank=True, null=True)
    sep_h_017_001 = models.IntegerField(blank=True, null=True)
    sep_h_018_001 = models.IntegerField(blank=True, null=True)
    sep_i_001_001 = models.TextField(blank=True, null=True)
    classificacao = models.CharField(max_length=1, blank=True, null=True)
    pais_sigla = models.CharField(max_length=60, blank=True, null=True)
    date_last_update = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        if(self.sep_a_001_001):
            return self.sep_a_001_001
        else:
            return str(self.prser_seq)
    
    class Meta:
        managed = False
        db_table = 'tb_prest_servico'
        verbose_name= 'Prestador de Serviço'
        verbose_name_plural = 'Prestadores de Serviço'


class TbDirRepresentante(models.Model):
    drep_seq = models.IntegerField(primary_key=True)
    prser_seq = models.ForeignKey('TbPrestServico', models.DO_NOTHING, db_column='prser_seq')
    sep_b_007_001 = models.CharField(max_length=255)
    sep_b_009_001 = models.CharField(max_length=60, blank=True, null=True)
    sep_b_006_001 = models.ForeignKey('TbTaxoTermo', models.DO_NOTHING, db_column='sep_b_006_001',related_name="TbDirRepresentante.sep_b_006_001+")
    sep_b_008_001 = models.ForeignKey('TbTaxoTermo', models.DO_NOTHING, db_column='sep_b_008_001')

    class Meta:
        managed = False
        db_table = 'tb_dir_representante'
        
    def __str__(self):
        if(self.sep_b_007_001):
            return self.sep_b_007_001
        else:
            return str(self.drep_seq)

class TbDirTecnico(models.Model):
    dtec_seq = models.IntegerField(primary_key=True)
    prser_seq = models.ForeignKey('TbPrestServico', models.DO_NOTHING, db_column='prser_seq')
    sep_b_011_001 = models.CharField(max_length=255)
    sep_b_013_001 = models.CharField(max_length=60, blank=True, null=True)
    sep_b_010_001 = models.ForeignKey('TbTaxoTermo', models.DO_NOTHING, db_column='sep_b_010_001',related_name="TbDirTecnico.sep_b_010_001+")
    sep_b_012_001 = models.ForeignKey('TbTaxoTermo', models.DO_NOTHING, db_column='sep_b_012_001')

    class Meta:
        managed = False
        db_table = 'tb_dir_tecnico'

    def __str__(self):
        if(self.sep_b_011_001):
            return self.sep_b_011_001
        else:
            return str(self.dtec_seq)



class TbTaxoTermo(models.Model):
    taxt_seq = models.IntegerField(primary_key=True)
    taxv_seq = models.ForeignKey('TbTaxoVocab', models.DO_NOTHING, db_column='taxv_seq')
    taxt_nome = models.CharField(max_length=255)
    taxt_descricao = models.CharField(max_length=4000, blank=True, null=True)
    taxt_pais = models.CharField(max_length=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tb_taxo_termo'
    
    def __str__(self):
        if(self.taxt_nome):
            return self.taxt_nome
        else:
            return str(self.taxt_seq)


class TbTaxoVocab(models.Model):
    taxv_seq = models.IntegerField()
    taxv_nome = models.CharField(max_length=255)
    taxv_descricao = models.CharField(max_length=4000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tb_taxo_vocab'
        


class TbTipoContexto(models.Model):
        
    PREFIX_CHOICES = (
        ('COM','Comunidade'),
        ('SYS','Sistema'),
        ('SEP','Prestador de Serviço'),
        ('TAP','PAT'),
    )
    tipo_con_seq = models.AutoField(primary_key=True)
    tipo_con_nome = models.CharField(max_length=255)
    tipo_con_seq_pai = models.ForeignKey('self', verbose_name="Contexto Pai",db_column='tipo_con_seq_pai', blank=True, null=True)
    tipo_con_codigo = models.CharField(max_length=10, verbose_name="Questionario", choices=PREFIX_CHOICES)

    
    def get_pai(self):
        if(self.tipo_con_seq_pai):
            return self.tipo_con_seq_pai.tipo_con_nome
        else:
            return ''
    get_pai.short_description = 'Contexto Pai'
    get_pai.admin_order_field = 'tipo_con_seq_pai__tipo_con_nome'
    
    class Meta:
        managed = False
        db_table = 'tb_tipo_contexto'
        verbose_name = 'Tipo Contexto'
        
    
    def __str__(self):
        return '' + self.tipo_con_nome


                
class TbUnit(models.Model):
    unit_seq = models.AutoField(primary_key=True)
    unit_label = models.CharField(max_length=255)
    unit_symbol = models.CharField(max_length=40, blank=True, null=True)
    unit_factor = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    unit_measure = models.CharField(max_length=50, blank=True, null=True)
    unit_module = models.CharField(max_length=150, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tb_unit'
        verbose_name = 'Unidade'
        ordering = ['unit_label']
        
    def __str__(self):
        return '' + self.unit_label


class TbPais(models.Model):
    pais_seq = models.AutoField(primary_key=True)
    pais_nome = models.CharField(max_length=600, blank=True, null=True)
    pais_abreviatura = models.CharField(max_length=600, blank=True, null=True)
    pais_sigla = models.CharField(max_length=10, blank=True, null=True)
    pais_capital = models.CharField(max_length=255, blank=True, null=True)
    pais_superficie_km2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    pais_populacao_hab = models.BigIntegerField(blank=True, null=True)
    pais_ano_informacao = models.CharField(max_length=4, blank=True, null=True)
    pais_forma_governo = models.CharField(max_length=255, blank=True, null=True)
    pais_densidade_hab_km2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    pais_idioma = models.CharField(max_length=100, blank=True, null=True)
    pais_moeda = models.CharField(max_length=100, blank=True, null=True)
    pais_idh = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    pais_idh_fonte_informacao = models.CharField(max_length=150, blank=True, null=True)
    pais_populacao_rural = models.BigIntegerField(blank=True, null=True)
    pais_num_comunidade = models.IntegerField(blank=True, null=True)
    pais_num_sistema = models.IntegerField(blank=True, null=True)
    pais_num_prest_servico = models.IntegerField(blank=True, null=True)

    def get_pais_siasar(self):
        return TbPais.objects.exclude(pais_capital__isnull=True).order_by('pais_sigla')

    class Meta:
        managed = False
        db_table = 'tb_pais'
        verbose_name = 'Pais'
        verbose_name_plural = "Paises"

    def __str__(self):
        return '' + self.pais_nome


