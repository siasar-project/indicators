from django.apps import AppConfig


class DadosConfig(AppConfig):
    name = 'siasar.dados'
    verbose_name = 'Dados Básicos'
