from django.contrib import admin

from siasar.dados.models import TbUnit, TbDivisaoAdmin, TbTipoContexto, \
    TbPrestServico, TbPat, TbDirRepresentante, TbDirTecnico, TbPais
from siasar.util import ReadOnlyAdmin, ReadOnlyTabularInline


class DivisaoAdminAdmin(ReadOnlyAdmin):
    search_fields = ('tda_nome','tda_pais')
    list_display = ('tda_nome','tda_pais',)
    raw_id_fields = ('tda_pai', )

class TipoContexoAdmin(admin.ModelAdmin):      
    list_display = ('__str__','get_pai',)

class RepresentanteInline(ReadOnlyTabularInline):
    model = TbDirRepresentante 

class TecnicosInline(ReadOnlyTabularInline):
    model = TbDirTecnico
    
class PrestadorServicoAdmin(ReadOnlyAdmin):
    inlines = [RepresentanteInline,TecnicosInline]
    search_fields = ('sep_a_001_001',)
    list_filter = ['pais_sigla']


# Register your models here.
admin.site.register(TbUnit,ReadOnlyAdmin)
admin.site.register(TbPrestServico,PrestadorServicoAdmin)
admin.site.register(TbPat,ReadOnlyAdmin)
admin.site.register(TbDivisaoAdmin,DivisaoAdminAdmin)
admin.site.register(TbTipoContexto,TipoContexoAdmin)
admin.site.register(TbPais,ReadOnlyAdmin)
