import datetime
import os
import signal
import subprocess
import threading
from os.path import join

from django.conf import settings
from django.db import models
from django.utils import timezone

from siasar.etl.utils import pid_exists

PENTAHO_PDI_PATH = getattr(settings, 'PENTAHO_PDI_PATH', '')
ETL_MAPS_PATH = getattr(settings, 'ETL_MAPS_PATH', '')
BASE_DIR = getattr(settings, 'BASE_DIR', '')
PENTAHO_LOG = getattr(settings, 'PENTAHO_LOG', '')
PENTAHO_REP = getattr(settings, 'PENTAHO_REP', '')


class MapasEtl(models.Model):
    TIPO_LOG_CHOICES = (
        ('Error', 'Error: Only show errors'),
        ('Nothing', 'Nothing: Dont show any output'),
        ('Minimal', 'Minimal: Only use minimal logging'),
        ('Basic', 'Basic: This is the default basic logging level'),
        ('Detailed', 'Detailed: Give detailed logging output'),
        ('Debug', 'Debug: For debugging purposes, very detailed output.'),
        ('Rowlevel', 'Rowlevel: Logging at a row level, this can generate a lot of data.'),
    )
    mapetl_seq = models.AutoField(primary_key=True)
    mapetl_nome = models.CharField(max_length=2000)
    mapetl_arquivo = models.CharField(max_length=2000)
    mapetl_cron = models.CharField(max_length=100)
    mapetl_parametro = models.CharField(max_length=4000,blank=True)
    mapetl_log = models.CharField(max_length=4000,blank=True, verbose_name="Log level", choices=TIPO_LOG_CHOICES)
    mapetl_ativo = models.BooleanField(blank=True)
    mapetl_dat_criacao = models.DateTimeField(auto_created=True, blank=False,default=datetime.datetime.now)
    mapetl_dat_ult_execucao = models.DateTimeField(blank=True)
    mapetl_imagem = models.ImageField(blank=True)
    mapetl_instalado = models.NullBooleanField(blank=True)
    mapetl_pid = models.IntegerField(blank=True,default=-1)
    mapetl_comando = models.CharField(max_length=4000, blank=True)

    def __str__(self):
        return '' + self.mapetl_arquivo.split('/')[-1]

    class Meta:
        app_label = 'etl'
        db_table = 'tb_mapa_etl'
        verbose_name = 'Mapas ETL'


    def execute_etl(self, start=True):

        if(not os.path.isdir(PENTAHO_PDI_PATH)):
            raise Exception("""Config Pentaho folder !""")
        elif (not os.path.isdir(ETL_MAPS_PATH)):
            raise Exception("""Config ETL folder !""")


        if(not start):
            try:
                os.kill(int(self.mapetl_pid), signal.SIGKILL)
                return ""
            except OSError as ex:
                raise Exception("""wasn't able to kill the process HINT:use signal.SIGKILL or signal.SIGABORT""")

        #print(join(ETL_MAPS_PATH,self.mapetl_arquivo))

        if('.kjb' in self.mapetl_arquivo):
            pentaho_exec = 'kitchen.sh'
        elif('.ktr' in self.mapetl_arquivo):
            pentaho_exec = 'pan.sh'
        else:
            raise Exception("""It is not a ETL Map !""")

        if(not self.mapetl_log):
            self.mapetl_log = 'Basic'

        parametro=[]
        if(self.mapetl_parametro):
            for p in self.mapetl_parametro.split(';'):
                if(p):
                    parametro.append('-param:' + str(p))

        command = [join(PENTAHO_PDI_PATH,pentaho_exec),
                '-file:'+join(ETL_MAPS_PATH,self.mapetl_arquivo),
                '-level:'+ self.mapetl_log,
                '-rep:'+PENTAHO_REP]

        command.extend(parametro)
        command.append('&')

        #print(command)
        class Exec(threading.Thread):
            def __init__(self, mapa_id, command, filename):
                self.mapa_id = mapa_id
                self.command = command
                self.f = open(filename, "w")
                threading.Thread.__init__(self)

            def run(self):
                mapa = MapasEtl.objects.get(mapetl_seq=self.mapa_id)
                mapa.mapetl_comando = ' '.join(self.command)
                p = subprocess.Popen(command,shell=False,stdout=self.f)
                mapa.mapetl_dat_ult_execucao = timezone.now()
                mapa.mapetl_pid = p.pid
                mapa.save()
                print("Saving log...")
                stdout, stderr = p.communicate()


            def stop(self):
                self.stopped = True

        filename = os.path.join(BASE_DIR,'media/log/log_'+str(self.mapetl_seq)+'.txt')
        execlass = Exec(mapa_id=self.mapetl_seq,command=command,filename=filename)
        if(not pid_exists(self.mapetl_pid)):
            execlass.start()
        else:
            raise Exception("Process still running...")

        return filename

