import kronos
from django.core.management.base import BaseCommand

from siasar.etl.cron import runetlmap
from siasar.etl.models import MapasEtl


class Command(BaseCommand):

    def handle(self, *args, **options):
        kronos.uninstall()
        kronos.registry = set()
        etl_maps = MapasEtl.objects.filter(mapetl_ativo=True)

        for me in etl_maps:
            params = me.mapetl_parametro.split(';') if me.mapetl_parametro or me.mapetl_parametro.strip()!='' else []
            params_args = {'id': me.mapetl_seq}
            for p in params:
                k = p.split('=')[0]
                v = p.split('=')[1]
                params_args[k] = v
            try:
                kronos.register(me.mapetl_cron,args=params_args)(runetlmap)
                me.mapetl_instalado = True
            except:
                me.mapetl_instalado = False

        kronos.install()