import sys
import traceback

from django import forms
from django.conf.urls import url
from django.contrib import admin
from django.contrib import messages
from django.core.management import call_command
from django.db.models import Case, Value, When
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.html import format_html

from siasar.etl.models import MapasEtl
from siasar.etl.utils import pid_exists, ImageWidget, list_maps_file
from siasar.etl.widgets import CronChar

mapa_imagewg = ImageWidget()


class MapsEtlForm(forms.ModelForm):
    mapetl_imagem = forms.ImageField(label='Map',widget=mapa_imagewg,required=False)
    mapetl_arquivo = forms.ChoiceField(choices=list_maps_file(),required=True,help_text='Select Map ETL',initial=False)
    mapetl_dat_ult_execucao = forms.DateTimeField(disabled=True,required=False)
    mapetl_dat_criacao = forms.DateField(disabled=True,required=False)
    mapetl_cron = forms.CharField(max_length=100,widget=CronChar())
    mapetl_pid = forms.CharField(disabled=True,required=False)
    mapetl_comando = forms.CharField(disabled=True,required=False)
    class Meta:
        model = MapasEtl
        exclude = []


@admin.register(MapasEtl)
class MapsEtlAdmin(admin.ModelAdmin):
    list_display = ('mapetl_nome','__str__','mapetl_cron', 'mapetl_ativo','mapetl_instalado','mapetl_dat_ult_execucao','image_tag','execute_actions')
    search_fields = ('mapetl_nome',)
    change_list_template = 'admin/siasar/etl/change_list.html'
    actions = ['make_ativo']

    def make_ativo(self, request, queryset):
        queryset.update(mapetl_ativo=Case(When(mapetl_ativo=True, then=Value(False)),default=Value(True)))

    make_ativo.short_description = "Ativar/Desativar ETL"

    form = MapsEtlForm

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                r'^(?P<mapetl_seq>.+)/etl/(?P<start>.+)$',
                self.admin_site.admin_view(self.process_execute),
                name='etl-execute',
            ),
        ]
        return custom_urls + urls

    def image_tag(self,obj):
        if(obj.mapetl_imagem):
            return u'<img width="100" src="%s" />' % obj.mapetl_imagem.url
        else:
            return u''
    image_tag.short_description = 'Map'
    image_tag.allow_tags = True

    def execute_actions(self,obj):
        if (not pid_exists(obj.mapetl_pid)):
            return format_html(
                '<a class="button" href="{}" style="background:#417690;">Execute</a>&nbsp;'
                '<a class="button" href="/media/log/log_'+str(obj.mapetl_seq)+'.txt">Log</a>&nbsp;',
                reverse('admin:etl-execute', args=[obj.mapetl_seq,1]),
            )
        else:
            return format_html(
                '<a class="button" href="{}" style="background:#ba2121;">Stop</a>&nbsp;'
                '<a class="button" href="/media/log/log_'+str(obj.mapetl_seq)+'.txt">Log</a>&nbsp;',
                reverse('admin:etl-execute', args=[obj.mapetl_seq,0]),
            )
    execute_actions.short_description = 'Actions'
    execute_actions.allow_tags = True

    def process_execute(self, request, mapetl_seq,start, *args, **kwargs):
        mapa_etl = self.get_object(request, mapetl_seq)
        start = bool(int(start))
        result = ''
        if request.method != 'POST':
            try:
                if(start):
                    self.message_user(request,'STARTED: '+mapa_etl.mapetl_nome)
                else:
                    self.message_user(request,'STOPED: '+mapa_etl.mapetl_nome)

                mapa_etl.execute_etl(start=start)
                url = reverse('admin:etl_mapasetl_changelist',args=[],current_app=self.admin_site.name,)
                return HttpResponseRedirect(url)
            except Exception as err:
                traceback.print_exc(file=sys.stdout)
                url = reverse('admin:etl_mapasetl_changelist', args=[], current_app=self.admin_site.name,)
                messages.error(request, 'ERRO: {0}'.format(err))
                return HttpResponseRedirect(url)

    def save_model(self, request, obj, form, change):
        super(MapsEtlAdmin, self).save_model(request, obj, form, change)
        if(obj.mapetl_instalado):
            call_command('uninstalltasks')
            call_command('loadtasks')
