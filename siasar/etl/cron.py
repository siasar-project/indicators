import kronos

from siasar.etl.models import MapasEtl
from siasar.settings import DEBUG


@kronos.register('0 0 1 1 *')
def runetlmap(args):
    if args:
        params = {p.split('=')[0]:p.split('=')[1] for p in args}
        if 'id' in params.keys():
            mapa_etl = MapasEtl.objects.filter(mapetl_seq=params['id']).first()
            print(mapa_etl.mapetl_nome)
            #if not DEBUG:
            mapa_etl.execute_etl(start=True)
            #else:
            #    print("Debug mode dont execute")