import traceback
import psutil
import re
import os

from collections import defaultdict
from pathlib import Path
from django.contrib.admin.widgets import AdminFileWidget
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe

from siasar.settings import ETL_MAPS_PATH


def searching_all_files(directory):
    dirpath = Path(directory)
    assert (dirpath.is_dir())
    file_list = []
    for x in dirpath.iterdir():
        # if x.is_file():
        file_list.append(x)
        if x.is_dir() and '.' not in x.name:
            file_list.extend(searching_all_files(x))

    return file_list


def list_maps_file():
    onlymaps = defaultdict(list)
    dirroot = ''
    resultfiles = [('---',[('','---')])]
    try:
        lstfile = searching_all_files(ETL_MAPS_PATH)
        lstfile.sort(key=lambda x: x.parent, reverse=True)
        for d in lstfile:
            dirnow = str(str(d.parent)).replace(ETL_MAPS_PATH, '')
            if (dirroot != dirnow):
                dirroot = dirnow
                if (d.is_dir()):
                    onlymaps[dirroot] = []
            elif (d.is_file()):
                if ('.k' in str(d) or '.js' in str(d)):
                    doc = str(d).replace(ETL_MAPS_PATH + os.sep,'')
                    onlymaps[dirroot].append((doc.replace(ETL_MAPS_PATH, ''), d.name))

        for dir, docs in onlymaps.items():
            finte = []
            for d in docs:
                finte.append(d)
            resultfiles.append((dir, finte))
    except:
        traceback.print_exc()

    return resultfiles


def pid_exists(pid):
    if psutil.pid_exists(pid):
        p = psutil.Process(pid)
        return p.status()!=psutil.STATUS_ZOMBIE and 'sh' in p.name()
    else:
        return False


def valida_cron(formula):
    rs = re.match('^(\?|\*|(?:[0-5]?\d)(?:(?:-|\/|\,)(?:[0-5]?\d))?(?:,(?:[0-5]?\d)(?:(?:-|\/|\,)(?:[0-5]?\d))?)*)\s+(\?|\*|(?:[01]?\d|2[0-3])(?:(?:-|\/|\,)(?:[01]?\d|2[0-3]))?(?:,(?:[01]?\d|2[0-3])(?:(?:-|\/|\,)(?:[01]?\d|2[0-3]))?)*)\s+(\?|\*|(?:0?[1-9]|[12]\d|3[01])(?:(?:-|\/|\,)(?:0?[1-9]|[12]\d|3[01]))?(?:,(?:0?[1-9]|[12]\d|3[01])(?:(?:-|\/|\,)(?:0?[1-9]|[12]\d|3[01]))?)*)\s+(\?|\*|(?:[1-9]|1[012])(?:(?:-|\/|\,)(?:[1-9]|1[012]))?(?:L|W)?(?:,(?:[1-9]|1[012])(?:(?:-|\/|\,)(?:[1-9]|1[012]))?(?:L|W)?)*|\?|\*|(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)(?:(?:-)(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))?(?:,(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)(?:(?:-)(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))?)*)\s+(\?|\*|(?:[0-6])(?:(?:-|\/|\,|#)(?:[0-6]))?(?:L)?(?:,(?:[0-6])(?:(?:-|\/|\,|#)(?:[0-6]))?(?:L)?)*|\?|\*|(?:MON|TUE|WED|THU|FRI|SAT|SUN)(?:(?:-)(?:MON|TUE|WED|THU|FRI|SAT|SUN))?(?:,(?:MON|TUE|WED|THU|FRI|SAT|SUN)(?:(?:-)(?:MON|TUE|WED|THU|FRI|SAT|SUN))?)*)(|\s)+$',formula,flags=re.IGNORECASE)
    if not rs:
        raise ValidationError("Erro ao validar a formula: %s" % formula)


class ImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        if (value):
            return mark_safe(u'''
                %s
                <img src="%s" />
            ''' % (super(ImageWidget, self).render(name, value, attrs), value.url))
        else:
            return super(ImageWidget, self).render(name, value, attrs)
