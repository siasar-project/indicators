# -*- coding: utf-8 -*-
#
# Created:    2018/26/01
# Author:         fabiano
#

from django import forms
from django.conf import settings
from django.utils.safestring import mark_safe

# set default settings
CRON_PATH = getattr(settings, 'CRON_PATH', 'cron')
if CRON_PATH.endswith('/'):
    CRON_PATH = CRON_PATH[:-1]

class CronChar(forms.TextInput):

    @property
    def media(self):
        js = ["%s/jqCron.js" % CRON_PATH,
              "%s/jqCron.pt_br.js" % CRON_PATH,
              "%s/etlCron.js" % CRON_PATH]
        css = ["%s/jqCron.css" % CRON_PATH]

        return forms.Media(
            css={
                'all': css
            },
            js=js
        )

    def __init__(self, **kwargs):
        super(CronChar, self).__init__(attrs={'class':'etl-cron','style':'display:none'}, **kwargs)

    def render(self, name, value, attrs=None):
        u"""Render CRONTextarea"""
        output = [super(CronChar, self).render(name, value, attrs),'']
        return mark_safe('\n'.join(output))
