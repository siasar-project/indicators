from django.db import models


# Create your models here.
class TbArmazenamento(models.Model):
    armaz_seq = models.BigIntegerField(primary_key=True)
    siste_seq = models.ForeignKey('TbSistema', models.DO_NOTHING, db_column='siste_seq', blank=True, null=True)
    sys_e_001_001 = models.CharField(max_length=100, blank=True, null=True)
    sys_e_002_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_e_003_001 = models.IntegerField(blank=True, null=True)
    sys_e_004_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_e_005_001 = models.IntegerField(blank=True, null=True)
    sys_e_006_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_e_007_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_e_008_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_e_009_001 = models.IntegerField(blank=True, null=True)
    sys_e_010_001 = models.TextField(blank=True, null=True)

    class Meta:
        app_label = 'sistema'
        managed = False
        db_table = 'tb_armazenamento'
        verbose_name = 'Armazenamento'
        
    def __str__(self):
        return self.sys_e_001_001+' - '+str(self.siste_seq)


class TbCaptacao(models.Model):
    capta_seq = models.BigIntegerField(primary_key=True)
    siste_seq = models.ForeignKey('TbSistema', models.DO_NOTHING, db_column='siste_seq', blank=True, null=True)
    sys_b_001_001 = models.TextField(blank=True, null=True)
    sys_b_002_001 = models.CharField(max_length=100, blank=True, null=True)
    sys_b_003_001 = models.IntegerField(blank=True, null=True)
    sys_b_004_001 = models.NullBooleanField()
    sys_b_005_001 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    sys_b_006_001 = models.IntegerField(blank=True, null=True)
    sys_b_007_001 = models.DateField(blank=True, null=True)
    sys_b_008_001 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    sys_b_009_001 = models.IntegerField(blank=True, null=True)
    sys_b_010_001 = models.DateField(blank=True, null=True)
    sys_b_011_001 = models.FloatField(blank=True, null=True)
    sys_b_012_001 = models.FloatField(blank=True, null=True)
    sys_b_013_001 = models.FloatField(blank=True, null=True)
    sys_b_014_001 = models.NullBooleanField()
    sys_b_015_001 = models.NullBooleanField()
    sys_b_016_001 = models.NullBooleanField()
    sys_b_017_001 = models.NullBooleanField()
    sys_b_018_001 = models.NullBooleanField()
    sys_b_019_001 = models.NullBooleanField()
    sys_b_020_001 = models.NullBooleanField()
    sys_b_021_001 = models.IntegerField(blank=True, null=True)
    sys_b_022_001 = models.TextField(blank=True, null=True)
    
    def __str__(self):
        return self.sys_b_001_001+' - '+str(self.siste_seq)
    
    class Meta:
        app_label='sistema'
        managed = False
        verbose_name = 'Captação'
        verbose_name_plural = 'Captações'
        db_table = 'tb_captacao'
    
    



class TbConducao(models.Model):
    condu_seq = models.BigIntegerField(primary_key=True)
    siste_seq = models.ForeignKey('TbSistema', models.DO_NOTHING, db_column='siste_seq', blank=True, null=True)
    sys_c_001_001 = models.CharField(max_length=100, blank=True, null=True)
    sys_c_002_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_c_003_001 = models.IntegerField(blank=True, null=True)
    sys_c_004_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_c_005_001 = models.IntegerField(blank=True, null=True)
    sys_c_006_001 = models.NullBooleanField()
    sys_c_007_001 = models.IntegerField(blank=True, null=True)
    sys_c_008_001 = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.sys_c_001_001+' - '+str(self.siste_seq)
    
    class Meta:
        app_label = 'sistema'
        managed = False
        db_table = 'tb_conducao'
        verbose_name = 'Candução'
        verbose_name_plural = 'Conduções'

class TbGrpDomicilio(models.Model):
    grpdo_seq = models.IntegerField(primary_key=True)
    comun_seq = models.ForeignKey('comunidade.TbComunidade', models.DO_NOTHING, db_column='comun_seq', blank=True, null=True)
    prser_seq = models.ForeignKey('dados.TbPrestServico', models.DO_NOTHING, db_column='prser_seq', blank=True, null=True)
    siste_seq = models.ForeignKey('TbSistema', models.DO_NOTHING, db_column='siste_seq', blank=True, null=True)
    grpdo_num_domic_atend = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tb_grp_domicilio'
        verbose_name = 'Grupo Domicílio'
    
    def __str__(self):
        nome_gd = ''
        if(self.comun_seq):
            nome_gd += str(self.comun_seq) + " / "
        else:
            nome_gd += "- / "
        if(self.siste_seq):
            nome_gd += str(self.siste_seq) + " / "
        else:
            nome_gd += "- / "
        
        if(self.prser_seq):
            nome_gd += str(self.prser_seq) + " / "
        else:
            nome_gd += "- / "
        
        return nome_gd
        
            
        return str(self.fonte_seq)
        
class TbFonteFinanc(models.Model):
    fonte_seq = models.BigIntegerField(primary_key=True)
    siste_seq = models.ForeignKey('TbSistema', models.DO_NOTHING, db_column='siste_seq', blank=True, null=True)
    sys_a_014_001 = models.IntegerField(blank=True, null=True)
    sys_a_015_001 = models.IntegerField(blank=True, null=True)
    sys_a_016_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_a_017_001 = models.IntegerField(blank=True, null=True)
    #sys_a_018_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)

    def __str__(self):
        return str(self.fonte_seq)
    
    class Meta:
        app_label = 'sistema'
        managed = False
        db_table = 'tb_fonte_financ'
        verbose_name = 'Fonte de Financiamento'
        verbose_name_plural = 'Fontes de Financiamentos'


class TbReabilAmpliacao(models.Model):
    reamp_seq = models.BigIntegerField(primary_key=True)
    siste_seq = models.ForeignKey('TbSistema', models.DO_NOTHING, db_column='siste_seq', blank=True, null=True)
    sys_a_019_001 = models.IntegerField(blank=True, null=True)
    sys_a_020_001 = models.IntegerField(blank=True, null=True)
    sys_a_021_001 = models.IntegerField(blank=True, null=True)
    sys_a_022_001 = models.IntegerField(blank=True, null=True)
    sys_a_023_001 = models.IntegerField(blank=True, null=True)
    sys_a_024_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_a_025_001 = models.IntegerField(blank=True, null=True)
    sys_a_026_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)

    def __str__(self):
        return str(self.reamp_seq)
    
    class Meta:
        app_label = 'sistema'
        managed = False
        db_table = 'tb_reabil_ampliacao'
        verbose_name = 'Reabilitação'
        verbose_name_plural = 'Reabilitações'
        
        
class TbRedeDistribuicao(models.Model):
    redis_seq = models.BigIntegerField(primary_key=True)
    siste_seq = models.ForeignKey('TbSistema', models.DO_NOTHING, db_column='siste_seq', blank=True, null=True)
    sys_f_001_001 = models.CharField(max_length=100, blank=True, null=True)
    sys_f_002_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_f_003_001 = models.IntegerField(blank=True, null=True)
    sys_f_004_001 = models.IntegerField(blank=True, null=True)
    sys_f_005_001 = models.IntegerField(blank=True, null=True)
    sys_f_006_001 = models.IntegerField(blank=True, null=True)
    sys_f_007_001 = models.IntegerField(blank=True, null=True)
    sys_f_008_001 = models.TextField(blank=True, null=True)

    def __str__(self):
        return str(self.sys_f_001_001)+' - '+str(self.siste_seq)
    
    class Meta:
        app_label = 'sistema'
        managed = False
        db_table = 'tb_rede_distribuicao'
        verbose_name = 'Rede de Distribuição'
        verbose_name_plural = 'Redes de Distribuição'



class TbSistema(models.Model):
    siste_seq = models.BigIntegerField(primary_key=True)
    validado = models.NullBooleanField()
    data_validacao = models.DateTimeField(blank=True, null=True)
    sys_a_001_001 = models.TextField(blank=True, null=True)
    sys_a_002_001 = models.IntegerField(blank=True, null=True)
    sys_a_004_001 = models.IntegerField(blank=True, null=True)
    sys_a_005_001 = models.IntegerField(blank=True, null=True)
    sys_a_006_001 = models.CharField(max_length=2, blank=True, null=True)
    sys_a_007_001 = models.FloatField(blank=True, null=True)
    sys_a_008_001 = models.FloatField(blank=True, null=True)
    sys_a_009_001 = models.FloatField(blank=True, null=True)
    sys_a_010_001 = models.CharField(max_length=100, blank=True, null=True)
    sys_a_011_001 = models.IntegerField(blank=True, null=True)
    sys_a_012_001 = models.IntegerField(blank=True, null=True)
    sys_a_013_001 = models.IntegerField(blank=True, null=True)
    sys_a_018_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_a_026_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_a_028_001 = models.NullBooleanField()
    sys_a_029_001 = models.NullBooleanField()
    sys_a_030_001 = models.BinaryField(blank=True, null=True)
    sys_a_031_001 = models.DateField(blank=True, null=True)
    sys_a_032_001 = models.CharField(max_length=255, blank=True, null=True)
    sys_g_001_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_g_002_001 = models.IntegerField(blank=True, null=True)
    sys_g_003_001 = models.IntegerField(blank=True, null=True)
    sys_g_004_001 = models.IntegerField(blank=True, null=True)
    sys_g_005_001 = models.DateField(blank=True, null=True)
    sys_g_006_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_g_007_001 = models.IntegerField(blank=True, null=True)
    sys_g_008_001 = models.DateField(blank=True, null=True)
    sys_g_009_001 = models.NullBooleanField()
    sys_g_010_001 = models.DateField(blank=True, null=True)
    sys_g_011_001 = models.NullBooleanField()
    sys_h_001_001 = models.TextField(blank=True, null=True)
    sys_a_034_001 = models.CharField(max_length=4000, blank=True, null=True)
    classificacao = models.CharField(max_length=1, blank=True, null=True)
    pais_sigla = models.CharField(max_length=60, blank=True, null=True)
    date_last_update = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.sys_a_001_001
    
    class Meta:
        app_label='sistema'
        managed = False
        db_table = 'tb_sistema'
        verbose_name='Sistema'

    def qntd_ar(self):
        return TbArmazenamento.objects.filter(siste_seq=self.siste_seq).count()
    qntd_ar.short_description = 'Qntd Armazenamento'

    def qntd_cp(self):
        return TbCaptacao.objects.filter(siste_seq=self.siste_seq).count()
    qntd_cp.short_description = 'Qntd Captações'

    def qntd_co(self):
        return TbConducao.objects.filter(siste_seq=self.siste_seq).count()
    qntd_co.short_description = 'Qntd Conduções'

    def qntd_rd(self):
        return TbRedeDistribuicao.objects.filter(siste_seq=self.siste_seq).count()
    qntd_rd.short_description = 'Qntd Rede Distribuicao'



class TbTipSistAbastecimento(models.Model):
    siste_seq = models.ForeignKey('TbSistema', models.DO_NOTHING, db_column='siste_seq')
    taxt_seq = models.ForeignKey('dados.TbTaxoTermo', models.DO_NOTHING, db_column='taxt_seq')
    sys_a_027_001 = models.IntegerField(blank=True, null=True)

    class Meta:
        app_label = 'sistema'
        managed = False
        verbose_name = 'Tipo Sistema Abastecimento'
        db_table = 'tb_tip_sist_abastecimento'
        unique_together = (('siste_seq', 'taxt_seq'),)
        
    def __str__(self):
        return self.taxt_seq.taxt_nome


class TbTratamento(models.Model):
    trata_seq = models.BigIntegerField(primary_key=True)
    siste_seq = models.ForeignKey('TbSistema', models.DO_NOTHING, db_column='siste_seq', blank=True, null=True)
    sys_d_001_001 = models.CharField(max_length=100, blank=True, null=True)
    sys_d_002_001 = models.IntegerField(blank=True, null=True)
    sys_d_003_001 = models.NullBooleanField()
    sys_d_004_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_d_005_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_d_006_001 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    sys_d_007_001 = models.IntegerField(blank=True, null=True)
    sys_d_008_001 = models.TextField(blank=True, null=True)

    def __str__(self):
        if(self.sys_d_001_001):
            return self.sys_d_001_001
        else:
            return self.sys_d_008_001
    
    class Meta:
        managed = False
        db_table = 'tb_tratamento'
        verbose_name = 'Tratamento'
    
        

