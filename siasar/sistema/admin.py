from django.contrib import admin

from siasar.sistema.models import TbSistema, TbArmazenamento, TbCaptacao, \
    TbConducao, TbFonteFinanc, TbReabilAmpliacao, \
    TbRedeDistribuicao, TbTratamento, TbGrpDomicilio
from siasar.util import ReadOnlyAdmin, ReadOnlyTabularInline


class ConducaoInline(ReadOnlyTabularInline):
    model = TbConducao

class ArmazenamentoInline(ReadOnlyTabularInline):
    model = TbArmazenamento

class CaptacaoInline(ReadOnlyTabularInline):
    model = TbCaptacao 
 
class FonteFinancInline(ReadOnlyTabularInline):
    model = TbFonteFinanc 

class RedeDistribuicaoInline(ReadOnlyTabularInline):
    model = TbRedeDistribuicao 

class ReabilAmpliacaoInline(ReadOnlyTabularInline):
    model = TbReabilAmpliacao 

class TratamentoInline(ReadOnlyTabularInline):
    model = TbTratamento 


class SistemaAdmin(ReadOnlyAdmin):
    inlines =[ArmazenamentoInline,CaptacaoInline,ConducaoInline,RedeDistribuicaoInline,ReabilAmpliacaoInline,TratamentoInline,FonteFinancInline]
    list_display = ('sys_a_001_001','sys_a_031_001','qntd_ar','qntd_cp','qntd_rd','qntd_co')
    search_fields = ('sys_a_001_001',)
    list_filter = ('pais_sigla',)
    
class GrupoDomicilioAdmin(ReadOnlyAdmin):
    list_display = ('comun_seq','siste_seq','prser_seq','grpdo_num_domic_atend')
    search_fields = ('comun_seq__com_a_003_001','siste_seq__sys_a_001_001','prser_seq__sep_a_001_001')
    list_filter = ('comun_seq__pais_sigla',)
    
    

# Register your models here.
admin.site.register(TbSistema,SistemaAdmin)
admin.site.register(TbArmazenamento,ReadOnlyAdmin)
admin.site.register(TbCaptacao,ReadOnlyAdmin)
admin.site.register(TbConducao,ReadOnlyAdmin)
#admin.site.register(TbFonteFinanc,ReadOnlyAdmin)
#admin.site.register(TbReabilAmpliacao,ReadOnlyAdmin)
admin.site.register(TbRedeDistribuicao,ReadOnlyAdmin)
admin.site.register(TbTratamento,ReadOnlyAdmin)
admin.site.register(TbGrpDomicilio,GrupoDomicilioAdmin)

#admin.site.register(TbCentroEducativo,ReadOnlyAdmin)