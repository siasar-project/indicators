from django.apps import AppConfig


class SistemaConfig(AppConfig):
    name = 'siasar.sistema'
    verbose_name = 'Sistema'
