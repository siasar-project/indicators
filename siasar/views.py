from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.core.management import call_command
from django.contrib import messages
from django.views.static import serve
from kronos.utils import read_crontab

from siasar.forms import PreferencesForm
from siasar.indicador.admin import clean_variavel_dw


def preferences(request):
    enable_kronos = False

    if request.method == 'POST':
        form = PreferencesForm(request.POST)

        if '_cleandw' in form.data.keys():
            if form.is_valid():
                try:
                    clean_variavel_dw(form.data['pais'])
                    messages.add_message(request, messages.INFO, 'DW Limpo para '+form.data['pais'])
                except:
                    messages.add_message(request, messages.ERROR, 'Erro ao limpara DW para ' + form.fields['pais'])
        elif '_installtasks' in form.data.keys():
            call_command('loadtasks')
            messages.add_message(request, messages.INFO, 'Crons Instaladas')
        elif '_removetasks' in form.data.keys():
            call_command('uninstalltasks')
            messages.add_message(request, messages.INFO, 'Crons Removidas')

    res = read_crontab()
    if ('kronos' in str(res)):
        enable_kronos = True
    form = PreferencesForm()
    return render(request, 'admin/siasar/preference.html', {'form': form,'enable_kronos':enable_kronos})


@login_required
def protected_serve(request, path, document_root=None, show_indexes=False):
    return serve(request, path, document_root, show_indexes)