'''
Created on 23 de nov de 2016

@author: fabianotavares
'''
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.db import connection
from django.db.utils import ProgrammingError, InternalError
from django.forms import widgets
from django.utils.safestring import mark_safe
import os
import subprocess
import re


from siasar.codemirror.widgets import CodeMirrorTextarea
from siasar.settings import BASE_DIR

from itertools import groupby
from django.forms.models import (
    ModelChoiceIterator, ModelChoiceField, ModelMultipleChoiceField
)


def valida_sql(sql_text):
    if(sql_text and sql_text!=''):
        sql_text = sql_text.replace("${periodo}","1990-01-01").lower()
        try:
            with connection.cursor() as cursor:
                print(sql_text)
                if("select" in sql_text):
                    if("delete" not in sql_text and "drop" not in sql_text and "update" not in sql_text and "insert" not in sql_text):
                        cursor.execute(sql_text+" LIMIT 1")
                        cursor.fetchone()
                    else:
                        raise ValidationError("Bad Words !")
                else:
                    raise ValidationError("SQL invalido")
        except Exception as err:
            raise ValidationError(err)


def valida_formula(formula):
    if(formula):
        res = avalia_expression(formula)
        if(res!="ok"):
            raise ValidationError("Erro ao validar a formula: %s"%res)

def avalia_expression(formula):
    formula = "java -jar  "+os.path.join(BASE_DIR, "java/calcpdi.jar") + " -a '%s'" % formula
    #print(formula)
    stdoutdata = subprocess.getoutput(formula)
    return stdoutdata

# your custom widget class
class SqlWidget(CodeMirrorTextarea):
    
    @property
    def media(self):
        media = super(SqlWidget, self).media
        #print(media)
        css = {
            'all': ('../static/css/sql.css',)
        }
        js = [
            '../static/js/sql.js',
        ]
        media.add_css(css)
        media.add_js(js)
        return media
        
        
    def render(self, name, value, attrs=None):
        return mark_safe(u'''
        <script type="text/javascript">
        function pop_sql() {
                popupCenter('/sql/play','Siasar Sql',''+ window.outerWidth /1.2,''+window.outerHeight / 1.2)
        }
        </script>
        <div>
            <button type="button" class="button_sql" onclick="pop_sql()"> Teste SQL</button>
            %s 
        </div>
        ''' % (super(SqlWidget, self).render(name, value, attrs)))
        


class ReadOnlyTabularInline(admin.TabularInline):
    extra = 0
    can_delete = False
    can_save = False
    editable_fields = []
    readonly_fields = []
    exclude = []

    def get_readonly_fields(self, request, obj=None):
        return list(self.readonly_fields) + \
               [field.name for field in self.model._meta.fields
                if field.name not in self.editable_fields and
                   field.name not in self.exclude]

    def has_add_permission(self, request):
        return False

class ReadOnlyAdmin(admin.ModelAdmin):
    readonly_fields = []

    def get_readonly_fields(self, request, obj=None):
        return list(self.readonly_fields) + \
               [field.name for field in obj._meta.fields]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
    
    class Media:
        css = {'all': ('/static/css/hide.css', )}


class Grouped(object):
    def __init__(self, queryset, group_by_field,
                 group_label=None, *args, **kwargs):
        """
        ``group_by_field`` is the name of a field on the model to use as
                           an optgroup.
        ``group_label`` is a function to return a label for each optgroup.
        """
        super(Grouped, self).__init__(queryset, *args, **kwargs)
        self.group_by_field = group_by_field
        if group_label is None:
            self.group_label = lambda group: group
        else:
            self.group_label = group_label

    def _get_choices(self):
        if hasattr(self, '_choices'):
            return self._choices
        return GroupedModelChoiceIterator(self)


class GroupedModelChoiceIterator(ModelChoiceIterator):
    def __iter__(self):
        if self.field.empty_label is not None:
            yield ("", self.field.empty_label)
        queryset = self.queryset.all()
        if not queryset._prefetch_related_lookups:
            queryset = queryset.iterator()
        for group, choices in groupby(self.queryset.all(),
                                      key=lambda row: getattr(row, self.field.group_by_field)):
            if self.field.group_label(group):
                yield (
                    self.field.group_label(group),
                    [self.choice(ch) for ch in choices]
                )


class GroupedModelChoiceField(Grouped, ModelChoiceField):
    choices = property(Grouped._get_choices, ModelChoiceField._set_choices)


class GroupedModelMultiChoiceField(Grouped, ModelMultipleChoiceField):
    choices = property(Grouped._get_choices, ModelMultipleChoiceField._set_choices)



if __name__ == '__main__':
    print(valida_formula('0 0 * * *'))