from controlcenter import widgets
from controlcenter.dashboards import Dashboard
from django.contrib.admin.models import LogEntry
from django.db.models.aggregates import Count
from django.db.models.expressions import RawSQL, F
from django.utils import timezone

from siasar.indicador.models import VariavelDw
from siasar.dados.models import TbPrestServico, TbPat, TbPais
from siasar.comunidade.models import TbComunidade
from siasar.sistema.models import TbSistema



def indicator_count_by_pais():
    pais = TbPais()
    paises = pais.get_pais_siasar()

    list_display = ['IAS', 'ISSA', 'NASH','ECS','EIA','NSA','NSH','PAT','PSE']

    result = []

    for p in paises:
        cr = {'pais': p.pais_abreviatura}
        ents = []
        for m in list_display:
            ents.append(VariavelDw.objects.filter(vardw_localidade__tda_pais=p.pais_sigla, varia_seq__varia_nome=m).values(pais_sigla=F('vardw_localidade__tda_pais')).annotate(total=Count('vardw_localidade__tda_pais')).order_by('-total'))
        i=0
        total_sum = 0
        for e in ents:
            c = next((x for x in e if x['pais_sigla'] == p.pais_sigla), None)
            total = c['total'] if c else 0
            cr[list_display[i]] = total
            i+=1
            total_sum += total
        cr['total'] = total_sum

        result.append(cr)

    return result

def entity_count_by_pais():
    pais = TbPais()
    paises = pais.get_pais_siasar()

    models = [TbComunidade, TbSistema, TbPrestServico, TbPat]
    ents = []

    for m in models:
        ents.append(m.objects.all().values('pais_sigla').annotate(total=Count('pais_sigla')).order_by('-total'))

    result = []
    list_display = ['COM', 'SIS', 'PSE', 'PAT']
    for p in paises:
        cr = {'pais': p.pais_abreviatura}
        i=0
        total_sum = 0
        for e in ents:
            c = next((x for x in e if x['pais_sigla'] == p.pais_sigla), None)
            total = c['total'] if c else 0
            cr[list_display[i]] = total
            i+=1
            total_sum += total
        cr['total'] = total_sum

        result.append(cr)

    return result


class CountEntityWidget(widgets.ItemList):
    title = 'Total de Questionários'
    sortable = True
    #list_display = ['Pais', 'Comunidades', 'Sistemas', 'Prestador de Serviço','PAT']
    list_display = ['pais', 'COM', 'SIS', 'PSE', 'PAT','total']
    limit_to = None
    width = widgets.LARGE

    def get_queryset(self):
        return entity_count_by_pais()


class CountIndicadoresWidget(widgets.ItemList):
    title = 'Total de Indicadores Calculados'
    sortable = True
    list_display = ['pais','IAS', 'ISSA', 'NASH','ECS','EIA','NSA','NSH','PAT','PSE','total']
    limit_to = None
    width = widgets.LARGE

    def get_queryset(self):
        return indicator_count_by_pais()



class ActionRecentWidget(widgets.ItemList):
    title = 'Ações Recentes'
    sortable = True
    model = LogEntry
    list_display = ['pk','object_repr']
    list_display_links = ['object_repr']

    width = widgets.MEDIUM

    def get_queryset(self):
        return LogEntry.objects.all()


class MainsDashboard(Dashboard):
    title = 'Siasar Dashboard'

    widgets = (
        CountEntityWidget,
        CountIndicadoresWidget
    )