from django.contrib import admin

from siasar.comunidade.models import TbComunidade, TbCentroSaude, \
    TbCentroEducativo
from siasar.util import ReadOnlyAdmin, ReadOnlyTabularInline


class CentroEducativoInline(ReadOnlyTabularInline):
    model = TbCentroEducativo 
    
class CentroSaudeInline(ReadOnlyTabularInline):
    model = TbCentroSaude

class ComunidadeAdmin(ReadOnlyAdmin):
    inlines =[CentroEducativoInline,CentroSaudeInline]
    list_display = ('com_a_003_001','qntd_ce','qntd_cs')   
    search_fields = ('com_a_003_001',)
    list_filter = ('pais_sigla',)
    
    

# Register your models here.
admin.site.register(TbComunidade,ComunidadeAdmin)
admin.site.register(TbCentroSaude,ReadOnlyAdmin)
admin.site.register(TbCentroEducativo,ReadOnlyAdmin)
