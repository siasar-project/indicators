from django.apps import AppConfig


class ComunidadeConfig(AppConfig):
    name = 'siasar.comunidade'
    verbose_name='Comunidade'
