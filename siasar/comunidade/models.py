from django.db import models


# Create your models here.
class TbComunidade(models.Model):
    comun_seq = models.BigIntegerField(primary_key=True)
    pat_seq = models.ForeignKey('dados.TbPat', models.DO_NOTHING, db_column='pat_seq', blank=True, null=True)
    validado = models.NullBooleanField()
    data_validacao = models.DateTimeField(blank=True, null=True)
    com_a_001_001 = models.DateField(blank=True, null=True)
    com_a_002_001 = models.CharField(max_length=100, blank=True, null=True)
    com_a_003_001 = models.CharField(max_length=255, verbose_name="Nome")
    com_a_004_001 = models.IntegerField(blank=True, null=True)
    com_a_005_001 = models.IntegerField(blank=True, null=True)
    com_a_006_001 = models.CharField(max_length=2, blank=True, null=True)
    com_a_007_001 = models.FloatField(blank=True, null=True)
    com_a_008_001 = models.FloatField(blank=True, null=True)
    com_a_009_001 = models.FloatField(blank=True, null=True)
    com_a_010_001 = models.CharField(max_length=30, blank=True, null=True)
    com_a_014_001 = models.IntegerField(blank=True, null=True)
    com_a_015_001 = models.IntegerField(blank=True, null=True)
    com_a_016_001 = models.IntegerField(blank=True, null=True)
    com_a_017_001 = models.TextField(blank=True, null=True)
    com_a_018_001 = models.IntegerField(blank=True, null=True)
    com_a_023_001 = models.IntegerField(blank=True, null=True)
    com_a_024_001 = models.NullBooleanField()
    com_a_025_001 = models.NullBooleanField()
    com_a_026_001 = models.NullBooleanField()
    com_a_027_001 = models.NullBooleanField()
    com_a_028_001 = models.TextField(blank=True, null=True)
    com_b_001_001 = models.NullBooleanField()
    com_b_002_001 = models.NullBooleanField()
    com_b_003_001 = models.NullBooleanField()
    com_b_004_001 = models.IntegerField(blank=True, null=True)
    com_b_005_001 = models.IntegerField(blank=True, null=True)
    com_b_006_001 = models.IntegerField(blank=True, null=True)
    com_b_007_001 = models.IntegerField(blank=True, null=True)
    com_b_008_001 = models.IntegerField(blank=True, null=True)
    com_b_009_001 = models.IntegerField(blank=True, null=True)
    com_b_010_001 = models.IntegerField(blank=True, null=True)
    com_b_011_001 = models.IntegerField(blank=True, null=True)
    com_b_012_001 = models.IntegerField(blank=True, null=True)
    com_b_013_001 = models.IntegerField(blank=True, null=True)
    com_b_014_001 = models.IntegerField(blank=True, null=True)
    com_b_015_001 = models.IntegerField(blank=True, null=True)
    com_b_016_001 = models.IntegerField(blank=True, null=True)
    com_b_017_001 = models.IntegerField(blank=True, null=True)
    com_b_018_001 = models.IntegerField(blank=True, null=True)
    com_b_019_001 = models.IntegerField(blank=True, null=True)
    com_b_020_001 = models.IntegerField(blank=True, null=True)
    com_b_021_001 = models.NullBooleanField()
    com_b_022_001 = models.IntegerField(blank=True, null=True)
    com_b_023_001 = models.IntegerField(blank=True, null=True)
    com_c_001_001 = models.NullBooleanField()
    com_c_002_001 = models.CharField(max_length=100, blank=True, null=True)
    com_c_003_001 = models.IntegerField(blank=True, null=True)
    com_d_001_001 = models.NullBooleanField()
    com_d_002_001 = models.TextField(blank=True, null=True)
    com_d_003_001 = models.TextField(blank=True, null=True)
    com_f_001_001 = models.TextField(blank=True, null=True)
    url_imagem = models.CharField(max_length=4000, blank=True, null=True)
    classificacao = models.CharField(max_length=1, blank=True, null=True)
    pais_sigla = models.CharField(max_length=60, blank=True, null=True)
    date_last_update = models.DateTimeField(blank=True, null=True)
    com_a_011_001 = models.IntegerField(blank=True, null=True)
    com_a_012_001 = models.IntegerField(blank=True, null=True)
    com_a_013_001 = models.IntegerField(blank=True, null=True)

    class Meta:
        app_label='comunidade'
        managed = False
        db_table = 'tb_comunidade'
        verbose_name = 'Comunidade'
        
    def __str__(self):
        return '' + self.com_a_003_001
    
    
    def qntd_ce(self):
        return TbCentroEducativo.objects.filter(comun_seq=self.comun_seq).count()
    qntd_ce.short_description = 'Qntd Centro Educativo'
    
    def qntd_cs(self):
        return TbCentroSaude.objects.filter(comun_seq=self.comun_seq).count()
    qntd_cs.short_description = 'Qntd Centro de Saúde'
    
    
    
class TbCentroEducativo(models.Model):
    cened_seq = models.IntegerField(primary_key=True)
    comun_seq = models.ForeignKey('comunidade.TbComunidade', models.DO_NOTHING, db_column='comun_seq', blank=True, null=True)
    siste_seq = models.ForeignKey('sistema.TbSistema', models.DO_NOTHING, db_column='siste_seq', blank=True, null=True)
    com_c_004_001 = models.TextField(blank=True, null=True)
    com_c_005_001 = models.CharField(max_length=255, blank=True, null=True)
    com_c_006_001 = models.IntegerField(blank=True, null=True)
    com_c_007_001 = models.IntegerField(blank=True, null=True)
    com_c_008_001 = models.IntegerField(blank=True, null=True)
    com_c_009_001 = models.IntegerField(blank=True, null=True)
    com_c_013_001 = models.IntegerField(blank=True, null=True)
    com_c_014_001 = models.TextField(blank=True, null=True)
    com_c_015_001 = models.IntegerField(blank=True, null=True)
    com_c_016_001 = models.IntegerField(blank=True, null=True)
    com_c_017_001 = models.IntegerField(blank=True, null=True)
    com_c_018_001 = models.IntegerField(blank=True, null=True)
    com_c_019_001 = models.IntegerField(blank=True, null=True)
    com_c_020_001 = models.IntegerField(blank=True, null=True)
    com_c_021_001 = models.IntegerField(blank=True, null=True)
    com_c_022_001 = models.IntegerField(blank=True, null=True)
    com_c_023_001 = models.IntegerField(blank=True, null=True)
    com_c_024_001 = models.IntegerField(blank=True, null=True)
    com_c_025_001 = models.IntegerField(blank=True, null=True)
    com_c_026_001 = models.IntegerField(blank=True, null=True)
    com_c_027_001 = models.IntegerField(blank=True, null=True)
    com_c_028_001 = models.IntegerField(blank=True, null=True)
    com_c_029_001 = models.IntegerField(blank=True, null=True)
    com_c_030_001 = models.IntegerField(blank=True, null=True)
    com_c_031_001 = models.IntegerField(blank=True, null=True)
    com_c_032_001 = models.IntegerField(blank=True, null=True)
    com_c_033_001 = models.IntegerField(blank=True, null=True)
    com_c_034_001 = models.IntegerField(blank=True, null=True)
    com_c_035_001 = models.IntegerField(blank=True, null=True)
    com_c_036_001 = models.IntegerField(blank=True, null=True)
    com_c_037_001 = models.IntegerField(blank=True, null=True)
    com_c_038_001 = models.IntegerField(blank=True, null=True)
    com_c_039_001 = models.IntegerField(blank=True, null=True)
    com_c_040_001 = models.IntegerField(blank=True, null=True)
    com_c_041_001 = models.IntegerField(blank=True, null=True)
    com_c_042_001 = models.IntegerField(blank=True, null=True)
    com_c_043_001 = models.IntegerField(blank=True, null=True)
    com_c_044_001 = models.IntegerField(blank=True, null=True)
    com_c_045_001 = models.IntegerField(blank=True, null=True)
    com_c_046_001 = models.IntegerField(blank=True, null=True)
    com_c_047_001 = models.IntegerField(blank=True, null=True)
    com_c_048_001 = models.IntegerField(blank=True, null=True)
    com_c_049_001 = models.IntegerField(blank=True, null=True)
    com_c_050_001 = models.IntegerField(blank=True, null=True)
    date_last_update = models.DateTimeField(blank=True, null=True)
    com_c_010_001 = models.CharField(max_length=200, blank=True, null=True)

    
    def __str__(self):
        if(self.com_c_004_001):
            return '' + self.com_c_004_001
        else:
            return str(self.cened_seq)

    class Meta:
        app_label='comunidade'
        managed = False
        db_table = 'tb_centro_educativo'
        verbose_name = 'Centro Educativo'

class TbCentroSaude(models.Model):
    censa_seq = models.IntegerField(primary_key=True)
    comun_seq = models.ForeignKey('TbComunidade', models.DO_NOTHING, db_column='comun_seq', blank=True, null=True)
    siste_seq = models.ForeignKey('sistema.TbSistema', models.DO_NOTHING, db_column='siste_seq', blank=True, null=True)
    com_d_004_001 = models.TextField(blank=True, null=True)
    com_d_005_001 = models.CharField(max_length=255, blank=True, null=True)
    com_d_006_001 = models.IntegerField(blank=True, null=True)
    com_d_007_001 = models.CharField(max_length=255, blank=True, null=True)
    com_d_008_001 = models.IntegerField(blank=True, null=True)
    com_d_009_001 = models.IntegerField(blank=True, null=True)
    com_d_010_001 = models.CharField(max_length=255, blank=True, null=True)
    com_d_012_001 = models.TextField(blank=True, null=True)
    com_d_013_001 = models.IntegerField(blank=True, null=True)
    com_d_014_001 = models.TextField(blank=True, null=True)
    com_d_015_001 = models.IntegerField(blank=True, null=True)
    com_d_016_001 = models.IntegerField(blank=True, null=True)
    com_d_017_001 = models.IntegerField(blank=True, null=True)
    com_d_018_001 = models.IntegerField(blank=True, null=True)
    com_d_019_001 = models.IntegerField(blank=True, null=True)
    com_d_020_001 = models.IntegerField(blank=True, null=True)
    com_d_021_001 = models.IntegerField(blank=True, null=True)
    com_d_022_001 = models.IntegerField(blank=True, null=True)
    com_d_023_001 = models.IntegerField(blank=True, null=True)
    com_d_024_001 = models.IntegerField(blank=True, null=True)
    com_d_025_001 = models.IntegerField(blank=True, null=True)
    com_d_026_001 = models.IntegerField(blank=True, null=True)
    com_d_027_001 = models.IntegerField(blank=True, null=True)
    com_d_028_001 = models.IntegerField(blank=True, null=True)
    com_d_029_001 = models.IntegerField(blank=True, null=True)
    com_d_030_001 = models.IntegerField(blank=True, null=True)
    com_d_031_001 = models.IntegerField(blank=True, null=True)
    com_d_032_001 = models.IntegerField(blank=True, null=True)
    com_d_033_001 = models.IntegerField(blank=True, null=True)
    com_d_034_001 = models.IntegerField(blank=True, null=True)
    com_d_035_001 = models.IntegerField(blank=True, null=True)
    com_d_036_001 = models.IntegerField(blank=True, null=True)
    com_d_037_001 = models.IntegerField(blank=True, null=True)
    com_d_038_001 = models.IntegerField(blank=True, null=True)
    com_d_039_001 = models.IntegerField(blank=True, null=True)
    com_d_040_001 = models.IntegerField(blank=True, null=True)
    com_d_041_001 = models.IntegerField(blank=True, null=True)
    com_d_042_001 = models.IntegerField(blank=True, null=True)
    com_d_043_001 = models.IntegerField(blank=True, null=True)
    com_d_044_001 = models.IntegerField(blank=True, null=True)
    com_d_045_001 = models.IntegerField(blank=True, null=True)
    com_d_046_001 = models.IntegerField(blank=True, null=True)
    com_d_047_001 = models.IntegerField(blank=True, null=True)
    com_d_048_001 = models.IntegerField(blank=True, null=True)
    com_d_049_001 = models.IntegerField(blank=True, null=True)
    com_d_050_001 = models.IntegerField(blank=True, null=True)
    date_last_update = models.DateTimeField(blank=True, null=True)
    com_d_011_001 = models.IntegerField(blank=True, null=True)
    com_d_025_001 = models.IntegerField(blank=True, null=True)

    def __str__(self):
        if(self.com_d_004_001):
            return '' + self.com_d_004_001
        else:
            return '' + str(self.censa_seq)
    
    class Meta:
        app_label='comunidade'
        managed = False
        db_table = 'tb_centro_saude'
        verbose_name = 'Centro de Saúde'
