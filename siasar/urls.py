"""siasar URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from controlcenter.views import controlcenter
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required
#from django.views.generic.base import TemplateView
#from rest_framework import routers
from django.conf.urls.static import static

from siasar import settings, views
#from siasar.indicador import views

admin.autodiscover()

#router = routers.DefaultRouter()
#router.register(r'variaveldw', views.VariavelDwViewSet)


urlpatterns = [
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^', include(admin.site.urls)),
    url(r'^sql/', include('explorer.urls')),
    url(r'^preferences/', login_required(views.preferences)),
    url(r'^dashboard/', controlcenter.urls),
    url(r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], views.protected_serve, {'document_root': settings.MEDIA_ROOT}),
    #url(r'^api/', include(router.urls)),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



