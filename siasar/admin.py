'''
Created on 20 de out de 2016

@author: fabianotavares
'''
from admin_interface.models import Theme
from django.contrib import admin
from explorer.models import Query
from django.contrib.admin import AdminSite
from django.views.decorators.cache import never_cache


# class SiasarAdminSite(AdminSite):
#     @never_cache
#     def index(self, request, extra_context=None):
#         pass
#
# admin.site = SiasarAdminSite()

admin.autodiscover()

admin.site.site_header = 'Sistema de Indicadores Siasar'
admin.site.site_title = 'Siasar'
admin.site.disable_action('delete_selected',)

admin.site.unregister(Query)
admin.site.unregister(Theme)


