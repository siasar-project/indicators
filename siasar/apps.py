from collections import OrderedDict

from controlcenter import app_settings
from django.apps import AppConfig
from django.core.exceptions import ImproperlyConfigured
from django.template.response import TemplateResponse
from django.utils.functional import cached_property
from django.utils.module_loading import import_string
from django.views.decorators.cache import never_cache


from siasar.settings import CONTROLCENTER_DASHBOARDS



class DefaultAppConfig(AppConfig):
    name = 'siasar'
    verbose_name= 'Siasar Indicador'

    def ready(self):
        from django.contrib import admin
        from django.contrib.admin import sites

        class SiasarAdminSite(admin.AdminSite):
            dashboard = None
            site_header = 'Dashboard Siasar'
            site_title = 'Dashboard Siasar'

            @cached_property
            def dashboards(self):
                dashboards = OrderedDict()
                for slug, path in enumerate(CONTROLCENTER_DASHBOARDS):
                    if isinstance(path, (list, tuple)):
                        slug, path = path
                    pk = str(slug)
                    klass = import_string(path)
                    dashboards[pk] = klass(pk=pk)

                if not dashboards:
                    raise ImproperlyConfigured('No dashboards found.')
                return dashboards

            @never_cache
            def index(self, request, extra_context=None):

                app_list = self.get_app_list(request)
                self.dashboard = next(iter(self.dashboards.values()))

                context = dict(
                    self.each_context(request),
                    title= self.site_header,
                    app_list=app_list,
                    dashboard=self.dashboard,
                    dashboards=self.dashboards.values(),
                    groups=self.dashboard.get_widgets(request),
                    sharp='#',
                )
                context.update(extra_context or {})

                request.current_app = self.name

                return TemplateResponse(request, self.index_template or 'admin/index.html', context)

        siasar = SiasarAdminSite()
        admin.site = siasar
        sites.site = siasar